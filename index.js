// Toán tử (tiền tố, hậu tố) ++ và -- trong js cũng như ngôn ngữ lập trình khác
/*
    B1: cộng 1 cho biến
    B2: trả về sau khi cộng
*/
var first = 6;
var outfirst = ++first;
console.log(outfirst);

/*
    B1: copy biến
    B2: cộng 1 cho biến
    B3: trả về biến đã copy
*/
var last = 6;
var outlast = last++;
console.log(outlast);

// Example
var sampleNumber = 10;
var outSampleNumber = sampleNumber++ - --sampleNumber;
console.log(outSampleNumber);